/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nebuleuse.ORM.xml;

import java.util.ArrayList;
import java.util.Iterator;





public class DocXML {

    
    private NodoXML root ;
    private ArrayList<NodoXML> nodos = new ArrayList <NodoXML>() ;
  
       
           

    public  void normalizeDoc ( FactoryXML factory ){
        
        NodoXML nodo = new NodoXML();
        
        
        // para root <xxxx>
        if (factory.getLineas().size() > 0){
            
            String line = factory.getLineas().get(0).getLinea();
            
            line = line.replace("<", "");
            line = line.replace(">", "");

            NodoXML root = new NodoXML(); 
            
            root.setNombre(line);
            this.setRoot(root);
            
        }
        
         
        for (int x = 1; x < factory.getLineas().size(); x++) {
            
            String linea = factory.getLineas().get(x).getLinea();    

            nodo = this.contruirListaNodos (nodo, linea);            
        }        
        
    }
    
    
    
    
    
    
    
    
    public NodoXML contruirListaNodos ( NodoXML nodop,  String line ) {
    
        NodoXML ret = new NodoXML();
        
        Integer allave = this.contarChar(line, '<');
        Integer cllave = this.contarChar(line, '>');
        Integer igual = this.contarChar(line, '=');
        

        // si tiene una llave cada una
        if ( allave == 1 && cllave == 1 ) {
            
            line = line.replace("<", "");
            line = line.replace(">", "");

            // solo etiqueta simple <etiqueta>
            if (igual == 0 ){
                
                String primera = line.substring(0, 1);                      
                if (!(primera.equals("/"))) {   
                    
                    NodoXML var = new NodoXML() ;
                    var.setNombre(line); 
                    
                    ret = var;                    
                    this.getNodos().add(var);
                }                            
            }
            else{
                
                // falta un if para asegurar el 
                
                int j = line.indexOf(' ');
                String nombre = line.substring(0, j);
                line = line.replaceFirst(nombre, "").trim();                
                                
                NodoXML var = new NodoXML() ;
                var.setNombre(nombre); 
//System.err.println(nombre);                        
                
                var.addAtributos(line);
                           
                ret = var;                    
                this.getNodos().add(var);
        
            }            
        }
        else {
                        
            
            String firstChar = line.substring(0, 1);            
            String lastChar = line.substring(line.length() - 1);
            

            //  <xxxxxxxx             
            if ( allave == 1 && firstChar.equals("<") ) {

                //  <column8 
                if (igual == 0 ) {

                    NodoXML var = new NodoXML() ;                    
                    String nombre = line.substring(1, line.length());                    
                    var.setNombre(nombre);
                    
                    ret = var;                    
                }
                else {
                    //  <column8 name="rol"
                    
                    NodoXML var = new NodoXML() ;
                    
                    int j = line.indexOf(' ');
                    String nombre = line.substring(1, j);
                    line = line.replaceFirst("<"+nombre, "").trim();                     
                    
                    var.setNombre(nombre);
                    var.addAtributos(line);
                    
                    ret = var;                 
                   
  
                }                
            }
            else {
            //  
                String fin = line.substring( line.length() -2, line.length());     
                
                

                allave = this.contarChar(line, '<');
                cllave = this.contarChar(line, '>');
                igual = this.contarChar(line, '=');                
                
                // xxxx="xxxx"
                if ( allave == 0 && cllave == 0 && igual != 0 ){
                    
                    NodoXML var = new NodoXML() ;
                    var.setNombre( nodop.getNombre()  );
                    var.setAtributos( nodop.getAtributos() );
                                        
                    var.addAtributos(line);                    
                    ret = var;                 
                }
                else{

                    // xxxx="xxxx >"
                    if ( cllave == 1  && igual != 0 && !(fin.equals("/>")) ){                    
                        
                        NodoXML var = new NodoXML() ;
                        var.setNombre( nodop.getNombre()  );
                        var.setAtributos( nodop.getAtributos() );

                        var.addAtributos(line);    
                        
                        ret = var;                                         
                        this.getNodos().add(var);
                        
                        //System.err.println( line );
                    }
                   
                    
                    
                    // xxxx="xxxx />"
                    if ( cllave == 1  && igual != 0 && fin.equals("/>") ){                    
                        
                        NodoXML var = new NodoXML() ;
                        var.setNombre( nodop.getNombre()  );
                        var.setAtributos( nodop.getAtributos() );

                        var.addAtributos(line);    
                        
                        ret = var;                                         
                        this.getNodos().add(var);
                        
                    }
                    else{
                        
                        // />"
                        if ( allave == 0  && igual == 0 && fin.trim().equals("/>") ){                    

                            NodoXML var = new NodoXML() ;
                            var.setNombre( nodop.getNombre()  );
                            var.setAtributos( nodop.getAtributos() );
//                            var.addAtributos(line);    

                            ret = var;                                         
                            this.getNodos().add(var);
                            
                        }
                    }                    
                }                
            }            
        }
        
        
        return ret;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    public Integer  contarChar ( String line, Character e) {
    
        Integer ret = 0;
        for ( int n = 0; n < line.length (); n++ ) {            
            Character c = line.charAt(n);             
            if ( c.equals(e) ) {        
                ret++;
            }
        }
        return ret;    
    }
    
    
    
    
    
    
    


    public NodoXML getRoot() {
        return root;
    }

    public void setRoot(NodoXML root) {
        this.root = root;
    }

    public ArrayList<NodoXML> getNodos() {
        return nodos;
    }

    public void setNodos(ArrayList<NodoXML> nodos) {
        this.nodos = nodos;
    }

    

    
}





