/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.padron.migracion.seccional;

import java.io.File;


/**
 *
 * @author hugo
 */
public class Migracion {
    
    private String carpeta = "";
    private String[] ficheros ;
    

    public String getCarpeta() {
        return carpeta;
    }

    public void setCarpeta(String carpeta) {
        this.carpeta = carpeta;
    }

    public String[] getFicheros() {
        return ficheros;
    }

    public void setFicheros() {        
        File dir = new File(this.carpeta); 
        this.ficheros = dir.list();                
    }
 
 
    public String quitarExt(  String c) {                
        return c.substring( 0,  c.length() - 4);
    }
 
 
    
    
    
    
    public String  quitarpunto (String line) {
        
            String ret = "";
            String str = line;
            
            for (int n = 0; n <str.length(); n++ ) { 
                char c = str.charAt (n);                         
                if (!(c == '.')){                                      
                    ret = ret + c;
                }                
            }            
            return ret;
            
    }    
    
        
    
    
    
    public String  recorrerLinea (String line) {
        
        String ret = "";
        String str = line;
        char a = ';';

        for (int n = 0; n <str.length(); n++ ) { 

            char c = str.charAt (n);                         
            if (!(c == ' ')){                                      
                ret = ret + c;
            }
            else{
                if (!(c == a)){                                      
                    ret = ret + ";";
                }              
            }
            a = c;
        }            
        return ret;
    }    
              
    
    
    


    public String formatearLinea (String line, String ch) {
        
        String ret = line;
        Integer m = this.maxCh(line, ' ');
        String rep = " ";
        
        
        for (int i = m; i > 1; i-- ) {             
            
            String camb = rep.repeat(i);            
            ret = ret.replaceAll(rep.repeat(i), ch );     
            
        }
        
        return ret;
    }    
              
    
    

    
    public Integer maxCh (String line, char h) {
        
        Integer ret = 0;
        Integer max = 0;
        
        String str = line;
        //char a = ';';

        for (int n = 0; n <str.length(); n++ ) { 

            char c = str.charAt (n);                         
            if ((c == ' ')){                                      
                max++;                
                if (max > ret){
                    ret = max;
                }
            }
            else{
                max=0;
            }            
        }    
        return ret;
    }    
              

    
    
    
    
    
    public String  eliminarCaracteres (String line) {
        
        String ret = "";
        String str = line;
        char a = ';';

        for (int n = 0; n <str.length(); n++ ) { 

            char c = str.charAt (n);      
            
            
            if (c == ' '){                                      
                ret = ret + c;
            }
            else{
                
                if (c == '-'){                                      
                    ret = ret + "-&";
                }
                else{
                
                    if (Character.isDigit(c)){
                        ret = ret + c;
                    }                                  
                
                }

            }
            
        }            
        return ret;

    }    
              



    
}
