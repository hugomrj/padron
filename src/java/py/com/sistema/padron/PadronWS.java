/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package py.com.sistema.padron;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.MatrixParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.Persistencia;




/**
 * REST Web Service
 * @author hugo
 */


@Path("padron")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)



public class PadronWS {

    @Context
    private UriInfo context;    
    private Persistencia persistencia = new Persistencia();       
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();   
    private Response.Status status  = Response.Status.OK;
    
    String json = "";
    
    Padron com = new Padron();       
                         
    public PadronWS() {
    }

    

    
    @GET
    @Path("/cedula/{cedula}")
    public Response getcedula(                 
            @PathParam ("cedula") Integer cedula ) throws IOException, Exception {
                     
        
        PadronDAO dao = new PadronDAO();

        this.com = (Padron) dao.getCedula(cedula);  

        String json = gson.toJson(this.com);

        if (this.com == null){
            this.status = Response.Status.NO_CONTENT;                           
        }

        return Response
                .status( this.status )                             
                .entity(json)                        
                .build();       
        
      
        
    }    
      

         

    
    
}