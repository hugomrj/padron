/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sistema.padron;




import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import nebuleuse.ORM.Persistencia;
import nebuleuse.util.Cadena;


public class PadronDAO  {
        
    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    private Gson gsonf = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
        
    
    public PadronDAO ( ) throws IOException  {

    }
       
    
    
    public void insert ( Padron padron, String linea ) throws Exception {    
        
        String line = linea;
        
        Integer orden = 0;
        
        Integer cedula = 0;
        String tmpcedula = "";
        
        String nombre = "";        
        String partido = "";
        String direccion = "";        
        
        System.out.println( line );              
        
        orden = Integer.parseInt( line.substring(0,3).trim() );              
        
        line = line.substring(3, line.length()).trim();
        
        // recorrer linea para obtener el resto de los campos
        
        boolean salir = false;        
        for (int n = 0; n < line.length() && (salir == false) ; n++ ) { 
            
            char c = line.charAt (n);                         
            
            //System.out.println( c );              
            
            if ( cedula == 0) {
                
                if (Cadena.isInt( Character.toString(c) )) {            
                    //System.out.print( c );      
                    tmpcedula = tmpcedula + c;
                }
                else if (c == ','){
                    //System.out.print( c ); 
                }
                else {
                    salir = true;
                    cedula = Integer.parseInt(tmpcedula);                                        
                    line = line.substring(n, line.length()).trim();                         
                }
            }            
        }             
        
        

        
        
      


        //nombre
        nombre = line.substring(0,line.indexOf("/")).trim();        
        line = line.substring( line.indexOf("/"), line.length()  ).trim();
        
        
        // partido
        salir = false;
        for (int n = 0; n < line.length() && (salir == false) ; n++ ) {         
            char c = line.charAt (n);      
            if (c != ' '){
                partido = partido + c;
            }
            else{
                salir = true;
                line = line.substring(n, line.length()).trim();         
                direccion = line.trim();
            }
        }

    padron.setLocal(133);
    padron.setOrden(orden);
    padron.setCedula(cedula);
    padron.setNombre(nombre);
    padron.setPartido(partido);
    padron.setDireccion(direccion);
        
    
        String sql = new PadronSQL().insert(padron);
        Integer cod  =  0;
        cod = persistencia.ejecutarSQL ( sql, "id") ;
    
        System.out.println( sql );        
          
        System.out.println( "" );        
    
    }
    



    
      public Padron getCedula(Integer cedula) throws Exception {      

          Padron padron = new Padron();                      
          String sql = (new PadronSQL().getCedula(cedula)); 
          
          padron = (Padron) persistencia.sqlToObject(sql, padron);          
          
          
          return padron;          
      }
                 
    



    
        
}
