/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sistema.padron;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import nebuleuse.ORM.sql.ReaderT;



/**
 *
 * @author hugo
 */
public class PadronSQL {
    

      
    
    
    public String insert ( Padron e ) {
        
        String sql = "sql";
        
                
        
        try {
        
            
            ReaderT readerSQL = new ReaderT("Padron");
            readerSQL.fileExt = "insert.sql";
            
            
            
            sql = readerSQL.get(
                    e.getLocal(),
                    e.getMesa(),
                    e.getOrden(),
                    e.getCedula(),
                    e.getNombre(),
                    e.getPartido(),
                    e.getDireccion()
            );
            
            
            
            
            
        } catch (IOException ex) {            
            System.err.println(ex.getMessage());
        }
        finally{
            return sql ;
        }
    }        
        
    
    public String getCedula (Integer cedula )
            throws Exception {
    
        String sql = "";                                 
        
        sql =   " SELECT *\n" +
                " FROM public.padron\n" +
                " WHERE cedula = " + cedula; 
        
        return sql ;      
    }
          
    
    
    
    
}




